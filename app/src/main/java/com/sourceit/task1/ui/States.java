package com.sourceit.task1.ui;

import android.content.res.Resources;

import com.sourceit.task1.App;
import com.sourceit.task1.R;

/**
 * Created by ${blcktm} on 13.02.2016.
 */
public class States {

    private static Resources res = App.getApp().getResources();

    public static final String WIFI_STRING = res.getString(R.string.state_wifi);
    public static final String GPS_STRING = res.getString(R.string.state_gps);
    public static final String AIR_STRING = res.getString(R.string.state_air);
    public static final String BLUETOOTH_STRING = res.getString(R.string.state_bluetooth);

    public static final int WI_FI = 0;
    public static final int BLUETOOTH = 1;
    public static final int GPS = 2;
    public static final int AIR_MODE = 3;

    public static final boolean ON = true;
    public static final boolean OFF = false;

    public static boolean[] states = new boolean[]{false, false, false, false};

}
